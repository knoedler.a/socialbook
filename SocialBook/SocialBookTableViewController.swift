//
//  SocialBookTableViewController.swift
//  SocialBook
//
//  Created by Anja Knödler on 15.11.19.
//  Copyright © 2019 Anja Knödler. All rights reserved.
//

import UIKit


class SocialBookTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var houseLabel: UILabel!
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var wandLabel: UILabel!
}

class SocialBookTableViewController: UITableViewController {
    
    var names : [String] = []
    var houses : [String] = []
    var images : [String] = []
    var wands : [String] = []

    var activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let connector = NetworkRequest(url: URL(string: "https://hp-api.herokuapp.com/api/characters")!)
        connector.fetch(viewController: self);
        
    }
    
    
    func showActivityIndicator() {
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        // minus height of navigation bar otherwise it's too low
        activityIndicator.center = CGPoint(x: UIScreen.main.bounds.size.width / 2, y: (UIScreen.main.bounds.size.height / 2) - (self.navigationController?.navigationBar.frame.height ?? 40))
        activityIndicator.color = #colorLiteral(red: 0.7333333333, green: 0.03921568627, blue: 0.1882352941, alpha: 1)
        tableView.addSubview(activityIndicator)
    }
    
    
    func fillTable(posts: [[String: Any]]) {

        if posts.count == 0 {
            showAlertView()
        }

        // loop through every post
        for post in posts {
            names.append(post["name"] as? String ?? "")
            houses.append("House: \(post["house"] as? String ?? "n/a")")
            // change http to https so image can be downloaded
            images.append((post["image"] as? String)?.replacingOccurrences(of: "http", with: "https") ?? "")
            
            if let wandInfo = post["wand"] as? [String: Any] {
                wands.append("Wand: \(wandInfo["wood"] as? String ?? "n/a"), \(wandInfo["core"] as? String ?? "n/a")")
            }
        }
        
        tableView.reloadData()
        
        activityIndicator.stopAnimating()
    }
    
    
    func showAlertView() {
        let alert = UIAlertController(title: "Achtung", message: "URL nicht korrekt, es können keine Daten angezeigt werden", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        names.count == 0 ? 0 :  names.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! SocialBookTableViewCell
        cell.selectionStyle = .none

        cell.nameLabel?.text = names[indexPath.row]
        cell.houseLabel?.text = houses[indexPath.row]
        
        cell.personImage?.image = UIImage(systemName: "photo")?.withRenderingMode(.alwaysTemplate)
        cell.personImage?.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
        // added self.images.count > indexPath.row because it crashed when an image was missing
        if !self.images.isEmpty && self.images.count > indexPath.row {
            cell.personImage?.downloaded(from: URL(string: self.images[indexPath.row])!)
        }
        
        cell.wandLabel?.text = wands[indexPath.row]
        cell.wandLabel?.numberOfLines = 2
        
        return cell
    }
}

// extension of UIImageView to load image from URL
extension UIImageView {

    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        URLSession.shared.dataTask( with: url as URL, completionHandler: {
           (data, response, error) -> Void in
           DispatchQueue.main.async {
              if let data = data {
                 self.image = UIImage(data: data)
              }
           }
        }).resume()
    }
}
