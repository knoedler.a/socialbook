//
//  Connector.swift
//  SocialBook
//
//  Created by Anja Knödler on 14.11.19.
//  Copyright © 2019 Anja Knödler. All rights reserved.
//

import Foundation

class NetworkRequest {
    let url : URL
    let session = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: .main)

    init(url: URL) {
        self.url = url
    }
    
    func execute (withCompletion completion: @escaping(Data?) -> Void) {
        let task = session.dataTask(with: url, completionHandler: {
            (data: Data?, _, _) -> Void in completion (data)
        })
        task.resume()
    }
    
    func fetch(viewController : SocialBookTableViewController){
        if !(self.url.absoluteString.isEmpty){
            viewController.showActivityIndicator()
            execute { [weak viewController] (data) in
                if let data = data {
                    viewController?.fillTable(posts: self.decodeData(data))
                }
            }
        } else {
            viewController.showAlertView()
        }
    }
    
    func decodeData (_ data: Data) -> [[String: Any]] {
        // serialize json data
        let json = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [[String:Any]]
        return json ?? []
    }
}
