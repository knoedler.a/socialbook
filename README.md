# SocialBook

## UI:
The App UI was built with StoryBoard. I did this because it’s easier to see how the app will look like when it’s finished and it’s possible to play with the looks of an application.
I chose a UITableViewController as the main view because all requirements from the task are fulfilled with this ViewController:
1. the entries are nicely shown below each other
2. all entries have the same height
3. the data gets updated when user scrolls

I’ve added a Navigation Controller so I could add the title „SocialBook“ above the TableView. Without the title the view looked crammed and not very nice.

## SocialBookTableViewController:
The ViewController starts with methods necessary for the table (numberOfSections, numberOfRowsInSection, cellForRowAt).
I’ve then extended the UIImageView so I can show images downloaded from a URL.
An Extension from SocialBookTableViewController holds the methods that fill the Table (fillTable), show the activity indicator (showActivityIndicator) and the method for showing the AlertView (showAlterView) that gets shown when something is not right with the URL or no data was received.


## Connector:
In here, the connection to fetch the data from the API is established. Also, the received data is decoded into a nested Dictionary for further use. I’ve created an extra class for this, so all network-related functionality is located at one place.

## Note:
Used API https://hp-api.herokuapp.com/api/characters
